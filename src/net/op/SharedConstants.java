package net.op;

public class SharedConstants {

	public static final String NAME = "OpenCraft";
	public static final String VERSION = "24r12";
	public static final String TECHNICAL_NAME = "LWJGL Edition";
	
	public static final String DISPLAY_NAME = NAME + ' ' + VERSION;
	
}
