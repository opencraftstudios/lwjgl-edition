<br />
<div align="center">
  <a href="https://github.com/OpenCraftStudios/LWJGL-OpenCraft">
    <img src="https://github.com/OpenCraftStudios/Java-Edition/blob/dev/.github/assets/images/icon.png?raw=true" alt="" height="128" width="128" />
  </a>

  <h3 align="center">OpenCraft</h3>

  <p align="center">
    A roughly calculated clone of Minecraft
    <br /><br />
    <a href="https://github.com/OpenCraftStudios/LWJGL-OpenCraft/wiki"><strong>Explore the docs »</strong></a>
    <br />
    <a href="https://github.com/OpenCraftStudios/LWJGL-OpenCraft/pulls">Report Bug</a>
  </p>
</div>

> [!WARNING]
> This project **IS NOT** affiliated with Mojang or other minecraft product. <br />
> This is just a hobby and I do not take care of any uses provided to this utility.

## About <img src="https://static-00.iconduck.com/assets.00/info-icon-2048x2048-tcgtx810.png" width="24" height="24" />
**OpenCraft** is a game programmed on Java and I want to create a community around this project,
I hope that you can be part of that community forking the repository or simply, bring us a star ♥️

## Prebuilt jars ☕
The prebuilt jars are in the [releases section](https://github.com/OpenCraftStudios/LWJGL-OpenCraft/releases).
Although you will need a launcher to play the game because it needs of libraries.

*By the way, those libraries are open-source and free, so you can download them easily.*

If you don't want to use a launcher, you can execute it manually. Only **make sure this**:

To execute the game:
```
java -jar OpenCraft.jar;mylibrary.jar; net.opencraft.OpenCraft
```

## Contributing ❤️
We are a small community so we need help to implement upgrades for new OpenCraft versions, fixing bugs and expanding the project.
We are always looking for motivated people to contribute. If you feel like it could be you, make sure these steps:
 - Select the type of download that you want:
    - [Beta version](https://github.com/OpenCraftStudios/LWJGL-OpenCraft/archive/refs/heads/master.zip)
    - [Stable version](https://github.com/OpenCraftStudios/LWJGL-OpenCraft/releases/latest)
 - Download a Java IDE:
   - [Eclipse IDE](https://www.eclipse.org/downloads/) (Light-weight, but for **experienced** programmers)
   - [Apache NetBeans](https://netbeans.apache.org/front/main/index.html) (Heavy, for amateurs)
   - [IntelliJ IDEA](https://www.jetbrains.com/idea/download/?section=windows) (For beginners!)

**Helps:**
 - [How I can make a fork?](https://docs.github.com/articles/fork-a-repo)
 - [How I can submit a pull request?](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/creating-a-pull-request)

## Java Docs (Coming soon!)

## Developers 👷‍♂️
- [Ciro Diaz](https://github.com/CiroZDP/)
- [The Nicolas Developer](https://github.com/TheNicolasDeveloper)

